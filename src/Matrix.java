import java.util.Arrays;

public class Matrix {

    private float[][] matrixArray;

    public Matrix(float[][] matrixArray) {
        this.matrixArray = matrixArray;
    }

    public Matrix(Matrix matrix) {
        this.matrixArray = matrix.matrixArray;
    }

    public Matrix(int n) {
        float[][] matrixArray = new float[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (j == i) {
                    matrixArray[i][j] = 1;
                }
            }
        }
        this.matrixArray = matrixArray;
    }

    public Matrix restrictMatrix(int x, int y) {
        int newLength = matrixArray.length - 1;
        float[][] copyArray = new float[newLength][newLength];
        for (int i = 0; i < matrixArray.length; i++) {
            for (int j = 0; j < matrixArray.length; j++) {
                if (i < x && j < y) {
                    copyArray[i][j] = matrixArray[i][j];
                }
                if (i < x && j > y) {
                    copyArray[i][j - 1] = matrixArray[i][j];
                }
                if (i > x && j < y) {
                    copyArray[i - 1][j] = matrixArray[i][j];
                }
                if (i > x && j > y) {
                    copyArray[i - 1][j - 1] = matrixArray[i][j];
                }
            }
        }
        return new Matrix(copyArray);
    }

    public float[][] getArray() {
        return this.matrixArray;
    }

    public float getValue(int x, int y) {
        return this.matrixArray[x][y];
    }

    public float getDeterminant() {
        float det = 0;

        for (int j = 0; j < this.matrixArray.length; j++) {
            Matrix m = this.restrictMatrix(0, j);
            if(this.matrixArray.length == 1) {
                float add = getValue(0,0);
                if(j % 2 != 0) {
                    add = -add;
                }
                det += add;
            } else {
                float add = getValue(0, j) * m.getDeterminant();
                if(j % 2 != 0) {
                    add = -add;
                }
                det += add;
            }
        }

        return det;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (float[] array : matrixArray) {
            s.append(Arrays.toString(array)).append("\n");
        }
        return s.toString();
    }
}
