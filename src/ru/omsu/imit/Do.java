package ru.omsu.imit;

public class Do {
    static double[] createVector(int size) {
        return new double[size];
    }

    public static double[][] createMatrix(int size) {
        double[][] array = new double[size][];
        for (int i = 0; i < size; i++) {
            array[i] = new double[size];
        }
        return array;
    }

    public static double[][] createUnitMatrix(int size) {
        double[][] array = createMatrix(size);
        for (int i = 0; i < array.length; i++) {
            array[i][i] = 1;
        }
        return array;
    }

    public static double[][] copyMatrix(double[][] origin) {
        int size = origin.length;
        double[][] temp = createMatrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                temp[i][j] = origin[i][j];
            }
        }
        return temp;
    }

    static double[][] triangleMatrix(double[][] arr) {
        for (int k = 0; k < arr.length; k++) {
            for (int i = k + 1; i < arr.length; i++) {
                double mu = arr[i][k] / arr[k][k];
                for (int j = 0; j < arr.length; j++) {
                    arr[i][j] -= arr[k][j] * mu;
                }
            }
        }
        return arr;
    }

    static double[][] swapColumns(double[][] arr, int i, int j) {
        for (int k = 0; k < arr.length; k++) {
            double tmp = arr[k][i];
            arr[k][i] = arr[k][j];
            arr[k][j] = tmp;
        }
        return arr;
    }

    static double[][] swapRows(double[][] arr, int i, int j) {
        double[] tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
        return arr;
    }

    static double[][] inverseMatrix(double[][] arr) {
        double[] tmp = new double[arr.length];
        for (int i = arr.length - 1; i >= 0; i--) {
            for (int j = i; j < arr.length; j++) {
                tmp[j] = arr[i][j];
            }

            arr[i][i] = 1. / arr[i][i];

            for (int j = i + 1; j < arr.length; j++) {
                double t = 0.;
                for(int k = i + 1; k <= j; k++) {
                    t += tmp[k] * arr[k][j];
                }
                arr[i][j] = -t / tmp[i];
            }
        }
        return arr;
    }

    static void printMatrix(double[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.print("\n");
        }
    }

    static void getReflection(double[][] a) {
        int size = a.length;
        double[] diag = createVector(size);
        double[] w = createVector(size);

        double length, length_2, diagElem, dotProd, lengthV;

        for (int j = 0; j < size - 1; j++) {
            // вычисляем w
            length_2 = 0;
            for (int i = j; i < size; i++) {
                w[i] = -a[i][j];
                length_2 += w[i] * w[i];
            }
            length = Math.sqrt(length_2);
            diagElem = a[j][j] > 0 ? -length : length;
            w[j] += diagElem;

            lengthV = Math.sqrt(2 * (length_2 + length * Math.abs(a[j][j])));
            for (int i = j; i < size; i++) {
                w[i] /= lengthV;
            }

            // применяем вектор w к столбцам матрицы
            for (int i = j; i < size; i++) {
                dotProd = 0.;
                for (int k = j; k < size; k++) {
                    dotProd += w[k] * a[k][i];
                }
                dotProd += dotProd;

                for (int k = j; k < size; k++) {
                    a[k][i] -= dotProd * w[k];
                }
            }

            // сохраняем w
            diag[j] = w[j];
            for (int i = j + 1; i < size; i++) {
                a[i][j] = w[i];
            }
        }

        // обратная верхнетреугольная
        inverseMatrix(a);

        // обратное отображение
        for (int j = size - 2; j >= 0; j--) {
            w[j] = diag[j];
            for (int i = j + 1; i < size; i++) {
                w[i] = a[i][j];
                a[i][j] = 0;
            }

            for (int i = 0; i < size; i++) {
                dotProd = 0;
                for (int k = j; k < size; k++) {
                    dotProd += w[k] * a[i][k];
                }
                dotProd += dotProd;

                for (int k = j; k < size; k++) {
                    a[i][k] -= dotProd * w[k];
                }
            }
        }
    }

    static double[][] matrixSub(double[][] A, double[][] B) {
        int size = A.length;
        double[][] C = createMatrix(size);

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                C[i][j] = A[i][j] - B[i][j];
            }
        }

        return C;
    }

    static double[][] matrixMult(double[][] A, double[][] B) {
        int size = A.length;
        double[][] C = createMatrix(size);

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                C[i][j] = 0;
                for (int k = 0; k < size; k++) {
                    C[i][j] += A[i][k] * B[k][j];
                }
            }
        }

        return C;
    }

    static double infinityNorm(double[][] a) {
        int size = a.length;
        double temp = 0;
        for (int i = 0; i < size; i++) {
            temp += Math.abs(a[i][0]);
        }
        for (int i = 1; i < size; i++) {
            double t = 0;
            for (int j = 0; j < size; j++) {
                t += Math.abs(a[i][j]);
            }
            if (temp < t && Math.abs(temp - t) < 1e-9) {
                temp = t;
            }
        }
        return temp;
    }
}
