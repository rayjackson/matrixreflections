package ru.omsu.imit;

import static ru.omsu.imit.Do.*;

public class Main {
    final static int MATRIX_SIZE = 100;

    static void getSymmetric(Gen gen, double[][] a, double[][] inv, int n, double alpha, double beta) {
        gen.mygen(a, inv, n, alpha, beta, 1, 2, 0, 1);
    }

    static void getSimpleStructure(Gen gen, double[][] a, double[][] inv, int n, double alpha, double beta) {
        gen.mygen(a, inv, n, alpha, beta, 1, 2, 1, 1);
    }

    static void getJordanCell(Gen gen, double[][] a, double[][] inv, int n, double alpha, double beta) {
        gen.mygen(a, inv, n, alpha, beta, 0, 0, 2, 1);
    }

    public static void main(String[] args) {
        int condition = 0;
        Gen gen = new Gen();

        double[][] A = createMatrix(MATRIX_SIZE);
        double[][] A_copy = createMatrix(MATRIX_SIZE);
        double[][] A_inv = createMatrix(MATRIX_SIZE);
        double[][] error = createMatrix(MATRIX_SIZE);
        double[][] residual = createMatrix(MATRIX_SIZE);
        double[][] E = createUnitMatrix(MATRIX_SIZE);

        for (double alpha = .1; alpha > 1e-17; alpha /= 10) {
            for (double beta = 1.; beta < 2.; beta++) {
                switch (condition) {
                    case 0:
                        getSymmetric(gen, A, A_inv, MATRIX_SIZE, alpha, beta);
                        break;
                    case 1:
                        getSimpleStructure(gen, A, A_inv, MATRIX_SIZE, alpha, beta);
                        break;
                    default:
                        getJordanCell(gen, A, A_inv, MATRIX_SIZE, alpha, beta);
                        break;
                }
                System.out.println("Alpha: " + alpha + ". Beta: " + beta);
//                printMatrix(A);
                A_copy = copyMatrix(A);
                getReflection(A);
                error = matrixSub(A, A_inv);
                residual = matrixSub(matrixMult(A_copy, A), E);

                double z = infinityNorm(error);
                double r = infinityNorm(residual);
                double zeta = z / infinityNorm(A_inv);

                System.out.println("||z|| = " + z);
                System.out.println("||r|| = " + r);
                System.out.println("||zeta|| = " + zeta);
                System.out.print("\n\n\n");
            }
        }
    }
}
